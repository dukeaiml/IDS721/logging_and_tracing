use lambda_runtime::{Error, LambdaEvent, service_fn, Context};
use serde::{Deserialize, Serialize};
use rand_distr::{Normal, Distribution};
use tracing_subscriber::fmt::format::FmtSpan;
use tracing::{info, instrument};
use rand::thread_rng;

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_span_events(FmtSpan::CLOSE)
        .init();

    let func = service_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

#[derive(Debug, Deserialize)]
struct CustomEvent {
    mean: f64,
    std_dev: f64,
}

#[derive(Serialize)]
struct CustomOutput {
    generated_number: f64,
}

#[instrument]
async fn func(event: LambdaEvent<CustomEvent>) -> Result<CustomOutput, Error> {
    let (event, _context) = event.into_parts();
    info!("Received event: {:?}", event);

    let normal = Normal::new(event.mean, event.std_dev).unwrap();
    let mut rng = thread_rng();
    let generated_number = normal.sample(&mut rng);

    Ok(CustomOutput { generated_number })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_lambda_function() {
        let test_event = CustomEvent {
            mean: 0.0,
            std_dev: 1.0,
        };
        let context = Context::default();

        let response = func(LambdaEvent::new(test_event, context)).await;
        assert!(response.is_ok());
        let output = response.unwrap();
        // Test output for validity rather than a fixed value due to randomness
        assert!(output.generated_number <= 3.0 && output.generated_number >= -3.0); // Example range, adjust as needed
    }
}

